﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace VAProjection
{
    public class Program
    {
        // メインメソッド
        private static void Main()
        {
            OptionPricing op = new OptionPricing();
            op.Run();
            Console.WriteLine("option price is {0}", op.price);
            Console.Read();
        }
    }
}