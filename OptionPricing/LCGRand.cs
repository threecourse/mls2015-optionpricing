﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VAProjection
{
    // 線形合同法による乱数生成
    // http://www.sat.t.u-tokyo.ac.jp/~omi/random_variables_generation.html#LCM
    class LCGRand
    {
        // 線形合同法のパラメータ
        int a = 1103515245;
        int b = 12345;
        int c = 2147483647;
        int x;

        // moroの方法のパラメータ
        double[] moro_a;
        double[] moro_b;
        double[] moro_c;

        // コンストラクタ（クラス生成時に必ず呼び出されるメソッド）
        public LCGRand()
        {
            // 線形合同法のパラメータ
            x = 71;

            // moroの方法のパラメータ
            moro_a = new double[9];
            moro_b = new double[9];
            moro_c = new double[9];
            moro_c[0] = 0.3374754822726147;
            moro_a[1] = 2.50662823884;
            moro_b[1] = -8.47351093090;
            moro_c[1] = 0.9761690190917186;
            moro_a[2] = -18.61500062529;
            moro_b[2] = 23.08336743743;
            moro_c[2] = 0.1607979714918209;
            moro_a[3] = 41.39119773534;
            moro_b[3] = -21.06224101826;
            moro_c[3] = 0.0276438810333863;
            moro_a[4] = -25.44106049637;
            moro_b[4] = 3.13082909833;
            moro_c[4] = 0.0038405729373609;
            moro_c[5] = 0.0003951896511919;
            moro_c[6] = 0.0000321767881768;
            moro_c[7] = 0.0000002888167364;
            moro_c[8] = 0.0000003960315187;

        }

        // 標準正規乱数を一つ返す
        public double Next()
        {
            x = (a * x + b) & c;
            double uniform = ((double)x + 1.0) / ((double)c + 2.0);
            return norms_inv(uniform);
        }

        // 標準正規乱数の配列を作成する
        public double[,] NextArray(int N, int M)
        {
            double[,] ret = new double[N, M];

            for (int n = 0; n < N; n++)
                for (int m = 0; m < M; m++)
                {
                    ret[n, m] = this.Next();
                }

            return ret;
        }

        // 標準正規乱数の累積分布関数の逆関数 - moroの方法による
        private double norms_inv(double x){

            if (x >= 1.0) x = 0.99999;
            if (x <= 0.0) x = 0.00001;
	
        	double y = x - 0.5;
	        double z = Math.Log(- Math.Log(0.5 - Math.Abs(y)));

            if (Math.Abs(y) <= 0.42){
	            double numer = y * Enumerable.Range(1, 4).Select(i => moro_a[i] * Math.Pow(y, 2 * (i - 1))).Sum();
	            double denom = 1.0 + Enumerable.Range(1, 4).Select(i => moro_b[i] * Math.Pow(y, 2 * i)).Sum();
	            return numer / denom;
            }
            else{
                return Math.Sign(y) * Enumerable.Range(0, 9).Select(i => moro_c[i] * Math.Pow(z, i)).Sum();
            }
        }
        
    }

}
