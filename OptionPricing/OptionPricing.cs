﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VAProjection
{
    // オプション価値を計算するクラス
    class OptionPricing
    {
        // シナリオの調整等を行っていないため、収束はあまり良くない

        public int Scenarios = 10000; //　シナリオ数
        int T = 10; // 満期までの期間
        int steps = 12; // タイムステップ

        public double[] value; // 各シナリオの結果
        public double price; // オプションの価値

        public void Run()
        {
            // パラメータのセット
            double r = 0.01;　// 連続金利、一定
            double sigma = 0.1; // ボラティリティ
            double K = 100; // 行使価格

            // TimeStep変数のセット
            int TT = T * steps;
            double deltaT = 1.0 / steps;

            // 結果の初期化
            value = new double[Scenarios];

            // 乱数の初期化
            LCGRand rand = new LCGRand();
            double[,] Z = rand.NextArray(Scenarios, TT); // scenario, tt

            // 各シナリオごとに計算
            for (int sc = 0; sc < Scenarios; sc++)
            {
                double S = 100.0; // 株価の初期値

                for (int tt = 0; tt < TT; tt++)
                {
                    double z = Z[sc, tt];
                    S *= Math.Exp((r - 1.0 / 2.0 * sigma * sigma) * deltaT + sigma * z * Math.Sqrt(deltaT));
                }

                value[sc] = Math.Exp(-r * T) * Math.Max(K - S, 0);
                if (sc % 100 == 0) { Console.WriteLine("scenario {0} finished", sc); }
            }

            price = value.Average();

        }

    }
}